import React from "react";
import "./Branch.css";

const Branch = (props) => {
  return (
    <div className="Branch">
      <h4>{props.country}</h4>
      <p> Номер Телефона {props.phone}</p>
      <p>Адрес: {props.address}</p>
    </div>
  );
};

export default Branch;
