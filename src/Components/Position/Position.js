import React from "react";
import "./Position.css";

const Position = (props) => {
  return (
    <div className="Position">
      <h6>{props.name}</h6>
      <p className="Person">{props.position}</p>
      <p>{props.text}</p>
    </div>
  );
};

export default Position;
