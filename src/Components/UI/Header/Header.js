import React from "react";
import { NavLink } from "react-router-dom";

import "./Header.css";

const Header = () => {
  return (
    <header className="Header">
      <div className="Container">
        <ul>
          <li>
            <NavLink
              exact
              to="/"
              activeStyle={{
                fontWeight: "bold",
                color: "white",
              }}
            >
              Главная
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/company"
              activeStyle={{
                fontWeight: "bold",
                color: "white",
              }}
            >
              О компании
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/shares"
              activeStyle={{
                fontWeight: "bold",
                color: "white",
              }}
            >
              Акции
            </NavLink>
          </li>
        </ul>
      </div>
    </header>
  );
};

export default Header;
